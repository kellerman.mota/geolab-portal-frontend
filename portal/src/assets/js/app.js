! function(e, t, o, a) {
  "use strict";

  o(function() {
    var e = o("body");
  })
}(window, document, window.jQuery),
  function(n, e, l, t) {
    "use strict";
    var c, r, d, u;

    function f(e) {
      e.siblings("li").removeClass("open").end().toggleClass("open")
    }

    function p() {
      l(".sidebar-subnav.nav-floating").remove(), l(".dropdown-backdrop").remove(), l(".sidebar li.open").removeClass("open")
    }

    function i() {
      return d.hasClass("aside-hover")
    }
    l(function() {
      c = l(n), r = l("html"), d = l("body");
      var t = (u = l(".sidebar")).find(".collapse");
      t.on("show.bs.collapse", function(e) {
        e.stopPropagation(), 0 === l(this).parents(".collapse").length && t.filter(".show").collapse("hide")
      });
      var e = l(".sidebar .active").parents("li");
      i() || e.addClass("active").children(".collapse").collapse("show");
      u.find("li > a + ul").on("show.bs.collapse", function(e) {
        i() && e.preventDefault()
      });
      var o = r.hasClass("touch") ? "click" : "mouseenter",
        a = l();
      u.on(o, ".sidebar-nav > li", function() {
        (d.hasClass("aside-collapsed") || d.hasClass("aside-collapsed-text") || i()) && (a.trigger("mouseleave"), a = function(e) {
          p();
          var t = e.children("ul");
          if (!t.length) return l();
          if (e.hasClass("open")) return f(e), l();
          var o = l(".aside-container"),
            a = l(".aside-inner"),
            n = parseInt(a.css("padding-top"), 0) + parseInt(o.css("padding-top"), 0),
            r = t.clone().appendTo(o);
          f(e);
          var i = e.position().top + n - u.scrollTop(),
            s = c.height();
          return r.addClass("nav-floating").css({
            position: d.hasClass("layout-fixed") ? "fixed" : "absolute",
            top: i,
            bottom: r.outerHeight(!0) + i > s ? 0 : "auto"
          }), r.on("mouseleave", function() {
            f(e), r.remove()
          }), r
        }(l(this)), l("<div/>", {
          class: "dropdown-backdrop"
        }).insertAfter(".aside-container").on("click mouseenter", function() {
          p()
        }))
      }), void 0 !== u.data("sidebarAnyclickClose") && l(".wrapper").on("click.sidebar", function(e) {
        if (d.hasClass("aside-toggled")) {
          var t = l(e.target);
          t.parents(".aside-container").length || t.is("#user-block-toggle") || t.parent().is("#user-block-toggle") || d.removeClass("aside-toggled")
        }
      })
    })
  }(window, document, window.jQuery),
  function(e, t, o, a) {
    "use strict";
    o(function() {
      o("[data-scrollable]").each(function() {
        var e = o(this);
        e.slimScroll({
          height: e.data("height") || 250
        })
      })
    })
  }(window, document, window.jQuery),

  function(l, e, c, t) {
    "use strict";
    c(function() {
      var i = c("body"),
        s = new o;
      c("[data-toggle-state]").on("click", function(e) {
        e.stopPropagation();
        var t = c(this),
          o = t.data("toggleState"),
          a = t.data("target"),
          n = void 0 !== t.attr("data-no-persist"),
          r = a ? c(a) : i;
        o && (r.hasClass(o) ? (r.removeClass(o), n) : (r.addClass(o), n )), c(l).resize()
      })
    });
    var o = function() {
      var a = "jq-toggleState";
    };
    l.StateToggler = o
  }(window, document, window.jQuery),
  function(o, a, n, e) {
    "use strict";
    n(function() {
      var e = n("[data-trigger-resize]"),
        t = e.data("triggerResize");
      e.on("click", function() {
        setTimeout(function() {
          var e = a.createEvent("UIEvents");
          e.initUIEvent("resize", !0, !1, o, 0), o.dispatchEvent(e)
        }, t || 300)
      })
    })
  }(window, document, window.jQuery),
  function(e, t, o, a) {
    "use strict";
    o(function() {
      o(".card.card-demo").on("card.refresh", function(e, t) {
        setTimeout(function() {
          t.removeSpinner()
        }, 3e3)
      }).on("hide.bs.collapse", function(e) {
        console.log("Card Collapse Hide")
      }).on("show.bs.collapse", function(e) {
        console.log("Card Collapse Show")
      }).on("card.remove", function(e, t, o) {
        console.log("Removing Card"), o.resolve()
      }).on("card.removed", function(e, t) {
        console.log("Removed Card")
      })
    })
  }(window, document, window.jQuery),

  function(e, t, o, a) {
    "use strict";
    (0, window.jQuery)(function() {})
  }(window, document);
