import { ValidadoresComponent } from './componentes/validador.component';
import { NgModule } from "@angular/core";
import { TextMaskModule} from "angular2-text-mask";
import { ChartModule } from "primeng/chart";
import {CalendarModule} from 'primeng/calendar';
import { CurrencyMaskModule } from "ng2-currency-mask";
import {
  CheckboxModule,
  ConfirmationService,
  ConfirmDialogModule,
  DataTableModule,
  DialogModule,
  DropdownModule,
  FileUploadModule,
  GrowlModule,
  InputMaskModule,
  InputTextareaModule,
  InputTextModule,
  SharedModule,
  SpinnerModule,
  TabViewModule,
  TooltipModule,
  PickListModule,
  ListboxModule,
  EditorModule,
  AccordionModule,
  AutoCompleteModule,
  StepsModule,
  BlockUIModule,
  RadioButtonModule, PasswordModule
} from "primeng/primeng";
import { EventoService } from "./servico";
import { CommonModule } from "@angular/common";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations"
import { RouterModule } from "@angular/router";
import { FormsModule } from "@angular/forms";
import { MensagemComponent } from "./componentes/mensagem.component";
import { LoadComponent } from "./componentes/load.component";
import { InputNumericComponent } from './componentes/input-numeric.component';
import { PanelComponent } from './componentes/view/panel/panel.component';
import { CnpjPipePipe } from './componentes/cnpj-pipe.pipe';
import {TableModule} from 'primeng/table';


@NgModule({
    imports: [
        DropdownModule,
        SpinnerModule,
        InputMaskModule,
        InputTextModule,
        FormsModule,
        RouterModule,
        TooltipModule,
        BrowserAnimationsModule,
        BrowserModule,
        CommonModule,
        SharedModule,
        DataTableModule,
        ConfirmDialogModule,
        GrowlModule,
        ListboxModule,
        InputTextareaModule,
        CalendarModule,
        FileUploadModule,
        TabViewModule,
        DialogModule,
        PickListModule,
        EditorModule,
        AccordionModule,
        AutoCompleteModule,
        StepsModule,
        BlockUIModule,
        TextMaskModule,
        ChartModule,
        RadioButtonModule,
        PasswordModule,
        TableModule,
        CurrencyMaskModule
    ],
    exports: [
        DropdownModule,
        SpinnerModule,
        InputMaskModule,
        InputTextModule,
        FormsModule,
        RouterModule,
        TooltipModule,
        BrowserModule,
        CommonModule,
        SharedModule,
        DataTableModule,
        ConfirmDialogModule,
        BrowserAnimationsModule,
        GrowlModule,
        MensagemComponent,
        LoadComponent,
        CheckboxModule,
        ListboxModule,
        InputTextareaModule,
        CalendarModule,
        FileUploadModule,
        TabViewModule,
        DialogModule,
        PickListModule,
        EditorModule,
        AccordionModule,
        AutoCompleteModule,
        StepsModule,
        BlockUIModule,
        TextMaskModule,
        InputNumericComponent,
        ChartModule,
        RadioButtonModule,
        PanelComponent,
        PasswordModule,
        CnpjPipePipe,
        TableModule,
        CurrencyMaskModule
    ],
    declarations: [
        MensagemComponent,
        LoadComponent,
        InputNumericComponent,
        PanelComponent,
        CnpjPipePipe
    ],
    providers: [
        EventoService,
        ConfirmationService,
        ValidadoresComponent,
        CnpjPipePipe
    ]
})
export class ArquiteturaModule { }
