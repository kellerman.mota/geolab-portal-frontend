import {Injectable, ViewChild} from "@angular/core";
import {EMPTY, Observable} from "rxjs";
import {EventoService} from "./servico/evento.service";
import {Router} from "@angular/router";
import {BlockUI} from "primeng/primeng";
import "rxjs/add/operator/finally"
import "rxjs/add/operator/catch"
import {
  HttpErrorResponse,
  HttpHandler, HttpHeaders,
  HttpInterceptor,
  HttpRequest, HttpResponse
} from "@angular/common/http";
import {LoginService, usuarioLogado} from "../login/login.service";
import {TipoMensagem} from "./modelo";
import {finalize, tap} from "rxjs/operators";

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  @ViewChild('p-blockui') block: BlockUI;

  constructor(
    private eventoService: EventoService,
    private router: Router,
    private auth: LoginService
  ) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler) {

    ///////////////////////////////////////////////
    let changedRequest = req;

    const headerSettings: { [name: string]: string | string[]; } = {};

    for (const key of req.headers.keys()) {
      headerSettings[key] = req.headers.getAll(key);
    }

    if (usuarioLogado()) {

      const authToken = JSON.parse(usuarioLogado()).token;

      headerSettings['Authorization'] = authToken;

    } else {
      this.verificarRetorno(req);
    }

    this.eventoService.openBlock();

    const newHeader = new HttpHeaders(headerSettings);

    changedRequest = req.clone({
      headers: newHeader
    });


    return next.handle(changedRequest)
      .pipe(
        tap(event => {
          if (event instanceof HttpResponse) {
            this.eventoService.closeBlock();
          }
        })
      )
      .catch((event) => {
        if (event.error instanceof Blob) {
          this.relatorioErro(event.error);
        }
        this.eventoService.closeBlock();
        if (event instanceof HttpErrorResponse) {
          return this.addMensagemError(event);
        }
      });
  }

  private verificarRetorno(req: HttpRequest<any>) {
    if (!this.ignorePublicEndpoints(req.url)) {
      this.auth.logout();
    }
  }

  private addMensagemError(event: HttpErrorResponse): Observable<any> {
    try {
      this.errorMessagem(event);
    } catch (e) {
      this.errorMessageStateZero(event);
    }

    if (event.status == 401) {
      this.auth.logout();
    }
    return EMPTY;
  }

  private errorMessageStateZero(event: HttpErrorResponse) {
    if (event.status == 0) {
      this.eventoService.emit({
        source: this,
        tipo: TipoMensagem.ERRO,
        mensagem: 'Ocorreu um erro ao acessar o servidor. Tente novamente.!'
      });

      this.auth.logout();
    }
  }

  private errorMessagem(event: HttpErrorResponse) {
    if (event.error.mensagem && event.status != 401) {
      this.eventoService.emit({source: this, tipo: TipoMensagem.ALERTA, mensagem: event.error.mensagem});
    }
  }

  private ignorePublicEndpoints(url: string): boolean {
    return url.indexOf('recuperar-senha') != -1 ||
      url.indexOf('login') != -1 ||
      url.indexOf('obterBaseEmailPorLogin') != -1 ||
      url.indexOf('auth') != -1;
  }

  relatorioErro(error: Blob) {
    let reader = new FileReader();
    reader.onload = (e: Event) => {
      try {
        const errmsg = JSON.parse((<any>e.target).result);
        this.eventoService.emit({source: this, tipo: TipoMensagem.ALERTA, mensagem: errmsg.mensagem});
      } catch (e) {
      }
    };
    reader.onerror = (e) => {
    };
    reader.readAsText(error);
  }


}
