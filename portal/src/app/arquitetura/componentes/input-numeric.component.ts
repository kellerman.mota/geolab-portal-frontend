
import { HostListener, Input, Component, Output, EventEmitter, OnInit, AfterViewChecked, AfterContentInit, AfterContentChecked, AfterViewInit } from "@angular/core";

@Component({
    selector: 'input-numeric',
    template: `

        <input type="text" class="form-control" [(ngModel)]="valorFormatado" maxlength="{{maxlength}}" disabled="{{disabled}}" #inputForm />

    `,
})
export class InputNumericComponent implements AfterViewInit {

    @Output() propertyChange = new EventEmitter<number>()

    @Input() property: any

    @Input() monetario: boolean = false

    @Input() percentual: boolean = false

    @Input() disabled: boolean = false

    @Input() maxlength: number

    valorFormatado: any = ''

    ngAfterViewInit() {

        setTimeout(() => {

            this.valorFormatado = this.property

            this.input()

        })

    }

    @HostListener('input') input() {

        this.formatarMonetario()

        this.formatarPercentual()
        
        this.formatarNumerico()

    }

    private formatarMonetario() {

        if (this.monetario) {

            /** DEFINE VALOR PARA O PROPERTY BINDING */
            this.property = this.obterNumero()

            this.propertyChange.emit(this.property)

            if (this.property) {
             
                /** FORMATA VALOR PARA VIEW */
                var numero = this.property.split('.')

                numero[0] = "R$ " + numero[0].split(/(?=(?:...)*$)/).join('.')

                this.valorFormatado = numero.join(',')
                
            }


        }

    }

    private formatarPercentual() {

        if (this.percentual) {

            /** DEFINE VALOR PARA O PROPERTY BINDING */
            this.property = this.obterNumero()

            this.propertyChange.emit(this.property)

            if (this.property) {

                /** FORMATA VALOR PARA VIEW */
                this.valorFormatado = this.property.replace(/[.]/g, ',')

            }

        }

    }

    private formatarNumerico() {

        if (!this.monetario && !this.percentual) {

            /** DEFINE VALOR PARA O PROPERTY BINDING */
            this.property = this.valorFormatado ? this.valorFormatado.toString().replace(/[^0-9]/g,'') : ''
    
            this.valorFormatado = this.valorFormatado ? this.valorFormatado.toString().replace(/[^0-9]/g,'') : ''

            this.propertyChange.emit(this.property)

        }

    }

    private obterNumero() {

        /** DEIXA APENAS NÚMEROS */
        let value = this.valorFormatado ? this.valorFormatado.toString().replace(/[^0-9]/g,'') : undefined

        if (value && value.length > 0 && Number.parseInt(value) > 0) {

            /** REMOVE ZEROS A ESQUERDA */
            let numero = Number.parseInt(value).toString();

            /** PREENCHE COM ZEROS A ESQUERDA CASO SEJA MENOR QUE 3 DIGITOS */
            while (numero && numero.length > 0 && numero.length < 3) {

                numero = '0' + numero

            }

            /** DEFINE VALOR PARA O PROPERTY BINDING */
            return numero.substring(0, numero.length - 2) + '.' + numero.substring(numero.length - 2, numero.length)

        } else {

            this.valorFormatado = ''

            return ''

        }

    }

}
