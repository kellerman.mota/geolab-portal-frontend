import { Router, ActivatedRoute } from "@angular/router";
import { OnInit, ViewChildren, QueryList } from "@angular/core";
import { ConfirmationService } from "primeng/primeng";

import { Entidade, Tipo } from "../modelo";
import { ManutencaoServico, EventoService } from "../servico";
import { BaseComponent } from "../componentes";
import { InputNumericComponent } from "./input-numeric.component";

/**
 * Classe que representa o componente base de manutenção das entidades
 */
export abstract class ManutencaoComponent<E extends Entidade> extends BaseComponent implements OnInit {

    @ViewChildren('inputForm') inputs: QueryList<any>;

    @ViewChildren(InputNumericComponent) inputsNumeric: QueryList<any>;

    menuCadastro: boolean = true;

    /**
     * Entidade que será mantida pelo componente
     */
    entidade: E;

    /**
     * Configura se é para voltar para a tela de listagem após a persistência (create, update)
     */
    protected voltarAposPersistencia: boolean = true;

    constructor(

        protected service: ManutencaoServico<E>,

        protected confirmationService: ConfirmationService,

        protected router: Router,

        protected activatedRoute: ActivatedRoute,

        eventoService: EventoService) {

        super(eventoService, activatedRoute);
    }

    /**
     * Método responsável por preparar entidade para salvar
     */
    preSalvar( entidade: E) {

        entidade.dataCadastro = new Date();

    }

    /**
     * Método responsável por preparar entidade para alterar
     */
    preAlterar(entidade: E) {

    }

    /**
     * Método responsável por salvar ou alterar uma determinada entidade
     */
    salvar(form?: any) {

      console.log(form);

        if (form && form.valid) {
          console.log(this.entidade);

            if (!this.entidade.id) {

                this.preSalvar(this.entidade);

                this.service.salvar(this.entidade).subscribe((retorno: any) => {

                    this.eventoService.persistenciaEventoEmit({ entidade: this.entidade, tipo: Tipo.INCLUSAO, mensagem: retorno.mensagem });

                    this.router.navigate(this.getVoltarCommand())
                })

            } else {

                this.preAlterar(this.entidade);

                this.service.alterar(this.entidade).subscribe((retorno: any) => {

                    this.eventoService.persistenciaEventoEmit({ entidade: this.entidade, tipo: Tipo.ALTERACAO, mensagem: retorno.mensagem });

                    this.router.navigate(this.getVoltarCommand())
                })
            }

        } else {

            this.adicionarMensagemAlerta('Campos obrigatórios (*) não informados');
        }
    }

    /**
     * Voltar para a tela de listagem
     */
    voltar() {

        if (this.isVisualizacao()) {

            this.router.navigate(this.getVoltarCommand());

        } else {

          this.router.navigate(this.getVoltarCommand())

         // this.confirmationService.confirm({

          //      message: 'Deseja realmente voltar para a listagem?',
         //       header: 'Confirmação',

          //      accept: () => {
          //        this.router.navigate(this.getVoltarCommand())

           //     }
           // });
            
        }
    }

    /**
     * Método responsável por criar uma nova entidade com o tipo parametrizado
     */
    protected abstract newEntidade(): E;

    /**
     * Método responsável por inicializar a manutenção
     */
    protected inicializarManutencao() {

        let id: number = this.activatedRoute.snapshot.params['id'];

        if (id) {

            this.service.get(id).subscribe((entidade) => {

                this.entidade = entidade;

                this['acaoAposCarregarEntidade'] && this['acaoAposCarregarEntidade']();

            });

        } else {

            this.entidade = this.newEntidade();
        }
    }

    /**
     * Método responsável por desativar campos de acordo com visualização/edição
     */
    renderizarDeAcordoComRequisicao() {

        this.inputs.changes.subscribe(e => {

            this.inputs.forEach(input => {
            
              try {
      
                input.nativeElement.disabled = this.isVisualizacao();
      
              } catch (Exception) {
      
                input.disabled = this.isVisualizacao();
      
              }
      
            })
      
        })

        this.inputsNumeric.changes.subscribe(e => this.inputsNumeric.forEach(input => input.disabled = this.isVisualizacao() ) )

    }

    /**
     * Inicialização do componente
     */
    ngOnInit() {

        super.ngOnInit();

        this.entidade = this.newEntidade();

        this.inicializarManutencao();

    }

    ngAfterViewInit(): void {

        this.renderizarDeAcordoComRequisicao()

    }

    /**
     * Método responsável por realizar a cópia de uma entidade.
     */
    protected copiarEntidade(entidade: E) {

        return <E>JSON.parse(JSON.stringify(entidade));

    }

    getPath() {

        return this.service.path;

    }

    /**
     * Método responsável por obter os commands para voltar para tela de listagem
     */
    getVoltarCommand(): any[] {

        return [this.getPath()];

    }

    /**
     * Método responsável por retornar os commands para abertura da tela de consulta
     */
    getConsultaCommand(): any[] {

        return [this.getPath()];

    }

    /**
     * Método responsável por retornar os commands para abertura da tela de cadastro
     */
    getCadastroCommand(entidade?: E): any[] {

        if (entidade) {

            return [this.getPath() + '/edicao/', entidade.id];

        } else {

            return [this.getPath() + '/novo'];
        }

    }
}
