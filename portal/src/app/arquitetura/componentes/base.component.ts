import { OnInit } from "@angular/core";
import { SelectItem } from "primeng/primeng";
import { Subscription } from "rxjs/Subscription";

import { EventoService } from "../servico/evento.service";
import { TipoMensagem } from "../modelo";
import { ActivatedRoute } from "@angular/router";

/**
 * Classe que compõe todos as funcionalidades base para os componentes
 */
export class BaseComponent implements OnInit {

    private subscricaoPersistenciaEvent: Subscription;

    tipoRequisicao: string = null;

    loading: boolean = false;

    maskTelefone = ['(', /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];

    maskCnpj = [/\d/, /\d/, '.', /\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/];

    maskCep = [/\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/];

    maskNumeric = [/\d/,/\d/,/\d/,/\d/];

    statusList: SelectItem[] = [
    
        { label: 'Todos', value: null },

        { label: 'Ativo', value: true },

        { label: 'Inativo', value: false }
    ]

    /**
     * Dados definidos para linguagem de dados
     */
    pt: any;

    constructor(

        protected eventoService: EventoService,

        protected activatedRoute: ActivatedRoute

    ) { }

    /**
     * Método responsável por adicionar mensagem informativa e propagá-la
     *
     * @param {string} mensagem a ser propagada como informativa
     */
    protected adicionarMensagemInformativa(mensagem: string) {

        this.eventoService.mensagemEvento.emit({

            mensagem: mensagem,

            source: this,

            tipo: TipoMensagem.INFORMATIVA
        })
    }

    /**
     * Método responsável por adicionar mensagem de erro e propagá-la
     *
     * @param {string} mensagem a ser propagada como de erro
     */
    protected adicionarMensagemErro(mensagem: string) {

        this.eventoService.mensagemEvento.emit({

            mensagem: mensagem,

            source: this,

            tipo: TipoMensagem.ERRO
        })
    }

    /**
     * Método responsável por adicionar mensagem de alerta e propagá-la
     *
     * @param {string} mensagem a ser propagada como de alerta
     */
    protected adicionarMensagemAlerta(mensagem: string) {

        this.eventoService.mensagemEvento.emit({

            mensagem: mensagem,

            source: this,

            tipo: TipoMensagem.ALERTA
        })
    }

    ngOnInit() {

        this.pt = {
          firstDayOfWeek: 0,
          dayNames: ["Domingo", "Segunda", "Terça", "Quarta", "Quinta", "Sexta", "Sábado"],
          dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
          dayNamesMin: ["D", "S", "T", "Q", "Q", "S", "S"],
          monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho',
            'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
          monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
          today: 'Hoje',
          clear: 'Limpar'
        };

        this.subscricaoPersistenciaEvent = this.eventoService.persistenciaEvento.subscribe((evento) => {

            if (evento.mensagem) {

                this.adicionarMensagemInformativa(evento.mensagem);
            }
        });

        var routerData = this.activatedRoute.snapshot.data;

        this.tipoRequisicao = routerData.tipoRequisicao;

    }

    ngOnDestroy() {

        // this.subscricaoPersistenciaEvent.unsubscribe();
    }

  parseDateHora(date: any) {
    if (date) {
      return new Date(date );
    }
    return null;
  }



  parseDate(date: any) {
    if (date) {
      return new Date(date + ' 00:00:00');
    }
    return null;
  }

  formatCnpj(value) {

    if (value) {

      value = new String(value);

      var bloco1: string = value.substring(0, 2);

      var bloco2: string = value.substring(2, 5);

      var bloco3: string = value.substring(5, 8);

      var bloco4: string = value.substring(8, 12);

      var bloco5: string = value.substring(12, 14);

      var cnpjFormatado: string = bloco1.concat(".").concat(bloco2).concat(".").concat(bloco3).concat("/").concat(bloco4).concat("-").concat(bloco5);

      return cnpjFormatado;

    }

    return null;
  }
    isListagem() {

        return this.tipoRequisicao == 'LISTAGEM';

    }

    isCadastro() {

        return this.tipoRequisicao == 'CADASTRO';

    }

    isEdicao() {

        return this.tipoRequisicao == 'EDICAO';

    }

    isVisualizacao() {

        return this.tipoRequisicao == 'VISUALIZACAO';

    }

}
