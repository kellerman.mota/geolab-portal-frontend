
import { Component } from "@angular/core";
import { EventoService } from "../servico";
import { MensagemEvent, TipoMensagem } from "../modelo";
import { Message } from 'primeng/primeng';
import { Subscription } from "rxjs/Subscription";

@Component({

    selector: 'load-block',

    template: `<p-blockUI style="
    z-index: 9999" [blocked]="contador > 0">
            <svg class="circular" viewBox="25 25 50 50">
                <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
            </svg>
        </p-blockUI>`
})
export class LoadComponent {

    subscricao: Subscription

    contador:  number = 0;

    constructor(private eventoService: EventoService) { }

    ngOnInit() {

        this.subscricao = this.eventoService.load.subscribe((retorno)=> {
            this.contador += retorno;
        });
    }

    ngDestroy() {

        this.subscricao.unsubscribe()
    }
}
