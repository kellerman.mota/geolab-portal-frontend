import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'cnpjPipe'
})
export class CnpjPipePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if (value) {

      value = new String(value);

      var bloco1: string = value.substring(0, 2);

      var bloco2: string = value.substring(2, 5);

      var bloco3: string = value.substring(5, 8);

      var bloco4: string = value.substring(8, 12);

      var bloco5: string = value.substring(12, 14);

      var cnpjFormatado: string = bloco1.concat(".").concat(bloco2).concat(".").concat(bloco3).concat("/").concat(bloco4).concat("-").concat(bloco5);

      return cnpjFormatado;

    }
  }

}
