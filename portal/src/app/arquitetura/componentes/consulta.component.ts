import { Router, ActivatedRoute } from "@angular/router";
import { ViewChild } from "@angular/core";
import { ConfirmationService, LazyLoadEvent, FilterMetadata, DataTable } from "primeng/primeng";

import { ManutencaoServico, EventoService } from "../servico";
import { BaseComponent } from "../componentes";
import { Entidade, Tipo } from "../modelo";
import { Paginacao } from "../modelo/paginacao/paginacao.dto";
import { PaginacaoResultado } from "../modelo/paginacao/paginacao-resultado.dto";
import { TipoOrdenacao } from "../modelo/paginacao/tipo-ordenacao.enum";
import { Filtro } from "../modelo/paginacao/filtro.dto";
import { Operacao } from "../modelo/paginacao/operacao.dto"

/**
 * Classe responsável por padronizar os componentes de consulta
 */
export abstract class ConsultaComponent<E extends Entidade> extends BaseComponent {

    @ViewChild('dt') dataTable: DataTable;

    paginacao: Paginacao<E>;

    resultado: PaginacaoResultado<E>;

    entidadeConsulta: E;

    menuConsulta: boolean = true;

    constructor(

        protected servico: ManutencaoServico<E>,

        protected confirmationService: ConfirmationService,

        protected router: Router,

        protected activatedRoute: ActivatedRoute,

        eventoService: EventoService) {

        super(eventoService, activatedRoute);

        this.paginacao = this.criarPaginacao();

        this.resultado = new PaginacaoResultado<E>()

        this.entidadeConsulta = this.newEntidade();
        
    }

    /**
     * Método responsável por criar a paginação padrão da consulta
     */
    protected criarPaginacao(): Paginacao<E> {

        return new Paginacao<E>()
    }

    /**
     * Método responsável por criar uma nova entidade com o tipo parametrizado
     */
    protected abstract newEntidade(): E;

    /**
     * Método responsável por prôver a funcionalidade de exclusão
     *
     * @param entidade entidade que será excluída
     */
    excluir(entidade: E) {

        this.confirmationService.confirm({

            message: 'Deseja realmente excluir o registro?',
            header: 'Confirmação',

            accept: () => {

                this.servico.excluir(entidade).subscribe((retorno: any) => {

                    this.listar();

                    this.eventoService.persistenciaEventoEmit({ entidade: entidade, tipo: Tipo.EXCLUSAO, mensagem: retorno.mensagem })
                })
            }
        })
    }


    /**
     * Método responsável por realizar a listagem via paginação
     */
    listar(event?: LazyLoadEvent) {
        
        this.loading = true;

        if (!this.paginacao) {

            this.paginacao = this.criarPaginacao();
        }

        this.paginacao.entidade = this.entidadeConsulta;

        this.paginacao.filtros = this.construirFiltroPesquisa();

        if (event) {

            this.paginacao.offset = event.first;

            this.paginacao.pageNumber = event.first == 0 ? 0 : event.first / event.rows;

            this.paginacao.pageSize = event.rows;

            this.paginacao.filtros = this.construirFiltroPesquisa();

            if (event.sortField) {

                this.paginacao.ordenacao = {

                    campo: event.sortField,

                    ordenacao: event.sortOrder < 0 ? TipoOrdenacao.DESC : TipoOrdenacao.ASC
                }
            }
        }

        console.log(this.paginacao);
        this.servico.listar(this.paginacao).subscribe((resultado) => {

            this.resultado = resultado as PaginacaoResultado<E>;

            this.eventoService.persistenciaEventoEmit({ tipo: Tipo.CONSULTA });

            this.loading = false;

        })
    }

    /**
     * Construir o filtro de pesquisa padrão
     */
    protected construirFiltroPesquisa(): Filtro[] {

        return [];

    }

    /**
     * Método responsável por criar os filtros de acordo com o objeto lazy do primeng
     * @param filtros
     */
    protected construirFiltro(filtros: { [s: string]: FilterMetadata }): Filtro[] {

        return Object.keys(filtros).map(propriedade => {

            let methodName = `filtro[${propriedade}]`;

            return this[methodName] && this[methodName](filtros[propriedade].value)

        })

            .filter(filtro => filtro != undefined)

            .map(filtro => filtro as Filtro[])

            .reduce((ant, atual) => ant.concat(atual), [])
    }

    /**
     * Método responsável por criar os filtros do campo id das entidades
     * @param value
     */
    protected 'filtro[id]'(value): Filtro[] {

        return [{

            campo: 'id',

            operacao: Operacao.EQ
        }]
    }

    /**
     * Método responsável por invocar o cadastro
     */
    cadastro(entidade?: E) {

        this.router.navigate(this.getCadastroCommand(entidade));
    }

    /**
     * Método responsável por invocar a visualizaçaõ
     */
    visualizar(entidade?: E) {

        this.router.navigate(this.getVisualizarCommand(entidade));
    }


    limparFiltros(): void {

        this.entidadeConsulta = this.newEntidade();

        this.listar();

    }

    getPath() {
        
        return this.servico.path;

    }

    /**
     * Método responsável por retornar os commands para abertura da tela de consulta
     */
    getConsultaCommand(): any[] {

        return [this.getPath()];

    }

    /**
     * Método responsável por retornar os commands para abertura da tela de visualização
     */
    getVisualizarCommand(entidade?: E): any[] {

        return [this.getPath() + '/visualizar/', entidade.id];

    }

    /**
     * Método responsável por retornar os commands para abertura da tela de cadastro
     */
    getCadastroCommand(entidade?: E): any[] {

        if (entidade) {

            return [this.getPath() + '/edicao/', entidade.id];

        } else {

            return [this.getPath() + '/novo'];
        }

    }

}
