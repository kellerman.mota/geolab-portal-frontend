import { Injectable } from '@angular/core';
import { TipoMensagem } from '../modelo';
import { EventoService } from '../servico';

export class ValidadoresComponent {

    constructor(

        @Injectable()
        protected eventoService: EventoService

    ) { }

    /**
     * Método responsável por adicionar mensagem de erro e propagá-la
     *
     * @param {string} mensagem a ser propagada como de erro
     */
    protected adicionarMensagemErro(mensagem: string) {

        this.eventoService.mensagemEvento.emit({

            mensagem: mensagem,

            source: this,

            tipo: TipoMensagem.ERRO
        })
    }

    mascaraCNPJ(cnpj) {

        if (cnpj) {

            cnpj = new String(cnpj);

            cnpj = cnpj.replace(/\D/g,"")
        
            cnpj = cnpj.replace(/^(\d{2})(\d)/,"$1.$2")
    
            cnpj = cnpj.replace(/^(\d{2})\.(\d{3})(\d)/,"$1.$2.$3")
    
            cnpj = cnpj.replace(/\.(\d{3})(\d)/,".$1/$2")
    
            cnpj = cnpj.replace(/(\d{4})(\d)/,"$1-$2")
        }


        return cnpj;
    }

    validarCNPJ(cnpj, mensagem) {

        cnpj = new String(cnpj);

        cnpj = cnpj.replace(/[^\d]+/g,'');

        if (cnpj == "00000000000000" || cnpj == "11111111111111" || 
                cnpj == "22222222222222" || cnpj == "33333333333333" || 
                    cnpj == "44444444444444" || cnpj == "55555555555555" || 
                        cnpj == "66666666666666" || cnpj == "77777777777777" || 
                            cnpj == "88888888888888" || cnpj == "99999999999999") {

            this.adicionarMensagemErro(mensagem);

            return true;
        }

        var tamanho = cnpj.length - 2
        var numeros = cnpj.substring(0,tamanho);
        var digitos = cnpj.substring(tamanho);
        var soma = 0;
        var pos = tamanho - 7;
        
        for (var i = tamanho; i >= 1; i--) {
            
            soma += numeros.charAt(tamanho - i) * pos--;
            
            if (pos < 2) {

                pos = 9;
            }
        }
        
        var resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
        
        if (resultado != digitos.charAt(0)) {

            this.adicionarMensagemErro(mensagem);

            return true;
        }
                
        tamanho = tamanho + 1;
        numeros = cnpj.substring(0,tamanho);
        soma = 0;
        pos = tamanho - 7;

        for (var i = tamanho; i >= 1; i--) {

            soma += numeros.charAt(tamanho - i) * pos--;
            
            if (pos < 2) {

                pos = 9;
            }
        }

        resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
        
        if (resultado != digitos.charAt(1)) {
    
            this.adicionarMensagemErro(mensagem);

            return true;
        }

        return false;
    }

    convertStringToDate(value: string) {

        var arrDataExclusao = value.split('/');
        
        var stringFormatada = arrDataExclusao[1] + '-' + arrDataExclusao[0] + '-' + arrDataExclusao[2];

        var dataFormatada = new Date(stringFormatada);

        return dataFormatada;
    }

    formatarDataTime(value: Date) {
        
        let dia = value.getDate() > 9 ? value.getDate().toString() : "0".concat(value.getDate().toString());

        let mes = (value.getMonth() + 1) > 9 ? value.getMonth() + 1 : "0".concat((value.getMonth() + 1).toString()) ;

        let ano = value.getFullYear();

        let hora = value.getHours() > 9 ? value.getHours().toString() : "0".concat(value.getHours().toString());

        let minuto = value.getMinutes() > 9 ? value.getMinutes().toString() : "0".concat(value.getMinutes().toString());

        let segundos = value.getSeconds() > 9 ? value.getSeconds().toString() : "0".concat(value.getSeconds().toString());

        let dataFormatada = dia + '/' + mes + '/' + ano + ' ' + hora + ':' + minuto + ':' + segundos;

        return dataFormatada;
    }

    formatarDate(value: Date) {
        
        let dia = value.getDate() > 9 ? value.getDate().toString() : "0".concat(value.getDate().toString());

        let mes = (value.getMonth() + 1) > 9 ? (value.getMonth() + 1).toString() : "0".concat((value.getMonth() + 1).toString()) ;

        let ano = value.getFullYear();

        let hora = value.getHours() > 9 ? value.getHours().toString() : "0".concat(value.getHours().toString());

        let minuto = value.getMinutes() > 9 ? value.getMinutes().toString() : "0".concat(value.getMinutes().toString());

        let segundos = value.getSeconds() > 9 ? value.getSeconds().toString() : "0".concat(value.getSeconds().toString());

        let dataFormatada = ano + mes + dia + hora + minuto + segundos;

        return dataFormatada;
    }

    formataDinheiro(n) {
        
        if (n) {

            return "R$ " + n.toFixed(2).replace('.', ',').replace(/(\d)(?=(\d{3})+\,)/g, "$1.");
        }

        return "R$ " + "0,00"
    }
    
    validarEmail(email) {
        
        let EMAIL_REGEXP = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        
        return EMAIL_REGEXP.test(email);
    
    }
}
