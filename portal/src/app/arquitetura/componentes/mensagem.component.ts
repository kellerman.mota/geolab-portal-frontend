
import { Component } from "@angular/core";
import { EventoService } from "../servico";
import { MensagemEvent, TipoMensagem } from "../modelo";
import { Message } from 'primeng/primeng';
import { Subscription } from "rxjs/Subscription";

@Component({

    selector: 'mensagem-manager',
    template: `<p-growl [life]="5000" [style]="{ 'width' : '25em' }" [(value)]="mensagens"></p-growl>`
})
export class MensagemComponent {

    subscricao: Subscription

    mensagens: Message[] = []

    constructor(private eventoService: EventoService) { }

    ngOnInit() {

        this.subscricao = this.eventoService.mensagemEvento.subscribe((mensagemEvento: MensagemEvent) => {

            switch (mensagemEvento.tipo) {

                case TipoMensagem.ALERTA: this.mensagens = [{ severity: 'warn', detail: mensagemEvento.mensagem }]; break;

                case TipoMensagem.ERRO: this.mensagens = [{ severity: 'error', detail: mensagemEvento.mensagem }]; break;

                case TipoMensagem.INFORMATIVA: this.mensagens = [{ severity: 'success', detail: mensagemEvento.mensagem }]; break;
            }

        })
    }

    ngDestroy() {

        this.subscricao.unsubscribe()
    }

}
