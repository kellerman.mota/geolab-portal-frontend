import { Route, Router } from "@angular/router";


export abstract class BaseRoute {

  constructor(router: Router) {

    const config = router.config;

    this.routes().forEach(r => config.push(r) )

    router.resetConfig(config);

  }

  protected abstract path(): string;

  protected abstract secao(): string;

  protected abstract modulo(): string;

  protected abstract menuLateralClass(): string;

  protected abstract listagemComponent();

  protected abstract manutencaoComponent();

  protected abstract getGuard();

  protected menu(): any {

    return {
  
      itens: [
        {
    
          nome: 'Consulta',
    
          destaca: false,
    
          route: this.path(),

          component: this.listagemComponent()
    
        },
        {
    
        nome: 'Cadastro',
    
        destaca: false,
    
        route: this.path() + '/novo',

        component: this.manutencaoComponent()
    
        },
      ]
  
    }

  }

  protected routes(): Route[] {

    return [

      { 
        path: this.path(), 

        component: this.listagemComponent(),

        canActivate: this.getGuard()
      },

      { 
        path: this.path() + '/novo', 

        component: this.manutencaoComponent(),

        canActivate: this.getGuard()

      },

      { 
        path: this.path() + '/edicao/:id', 

        component: this.manutencaoComponent(),

        canActivate: this.getGuard()

      },

      { 
        path: this.path() + '/visualizar/:id', 

        component: this.manutencaoComponent(),

        canActivate: this.getGuard()

      }

    ]

  }

}
