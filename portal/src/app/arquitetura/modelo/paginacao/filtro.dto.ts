import { Operacao } from "./operacao.dto";

/**
 * Filtro para uma determinada paginação. Exemplo:
 * 
 * let filtro = {
 * 
 *  campo: 'nome',
 * 
 *  tipo: TipoFiltro.STRING
 * 
 *  valor: {
 * 
 *      ILIKE: ['CEZAR']
 *  }
 * }
 */
export interface Filtro {

    campo: string;

    operacao: Operacao;
    
}