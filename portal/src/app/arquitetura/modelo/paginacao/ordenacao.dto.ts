import { TipoOrdenacao } from "./tipo-ordenacao.enum";


/**
 * Representa as ordenações que podem ser realizadas no projeto
 */
export interface Ordenacao {

    campo: string

    ordenacao: TipoOrdenacao
}