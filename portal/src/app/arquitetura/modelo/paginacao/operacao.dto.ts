
/**
 * Enumerações das operações disponíveis para a paginação dos resultados
 */
export enum Operacao {

    EQ,
    LIKE,
    ILIKE
    
}