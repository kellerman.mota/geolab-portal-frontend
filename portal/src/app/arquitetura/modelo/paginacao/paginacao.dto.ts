import { Filtro } from "./filtro.dto";
import { Ordenacao } from "./ordenacao.dto";
import { Entidade } from "..";

/**
 * Classe responsável pela configuração da paginação de uma determinada entidade
 */
export class Paginacao<E extends Entidade> {

    pageNumber: number;

    pageSize: number;

    offset: number;

    filtros: Filtro[];

    ordenacao : Ordenacao;

    entidade: E;

    constructor() {

        this.offset = 0;

        this.pageNumber = 0;

        this.pageSize = 10;

        this.filtros = [];

    }
}