
/**
 * Tipo de Ordenação da Paginação dos resultados
 */
export enum TipoOrdenacao {

    ASC, DESC
}