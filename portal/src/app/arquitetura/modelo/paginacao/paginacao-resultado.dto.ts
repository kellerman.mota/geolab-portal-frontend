import { Entidade } from "../entidade.model";

/**
 * Representa o resultado da paginação
 */
export class PaginacaoResultado<E extends Entidade> {

    content: E[];

    number: number;

    totalElements: number;

    totalPages: number;

    constructor() {

        this.content = [];

        this.number = 0;
    }
}