
import { Entidade } from "../modelo";

export interface PersistenciaEvento<E extends Entidade> {

    entidade?: E

    tipo: Tipo
    
    mensagem?: string
}

export enum Tipo {

    INCLUSAO, ALTERACAO, EXCLUSAO, CONSULTA
}