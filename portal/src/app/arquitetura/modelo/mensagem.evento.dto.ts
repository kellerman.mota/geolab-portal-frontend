export interface MensagemEvent {

    mensagem: string

    source: any

    tipo: TipoMensagem
}

export enum TipoMensagem {

    INFORMATIVA,

    ALERTA,

    ERRO
}