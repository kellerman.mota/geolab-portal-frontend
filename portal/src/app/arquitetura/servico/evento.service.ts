
import { MensagemEvent, PersistenciaEvento, Entidade } from "../modelo";
import { Injectable, Output, EventEmitter } from "@angular/core";

@Injectable()
export class EventoService {

    @Output() mensagemEvento = new EventEmitter<MensagemEvent>(true);

    @Output() persistenciaEvento = new EventEmitter<PersistenciaEvento<Entidade>>(true);

    @Output() load = new EventEmitter<number>(true);

    emit(mensagem: MensagemEvent) {

        this.mensagemEvento.emit(mensagem)
    }

    persistenciaEventoEmit(persistenciaEvento: PersistenciaEvento<Entidade>) {

        this.persistenciaEvento.emit(persistenciaEvento)
    }

    openBlock(){

        this.load.emit( + 1);
    }

    closeBlock(){

        this.load.emit( - 1)
    }
}
