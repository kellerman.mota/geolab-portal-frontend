
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import 'rxjs/add/operator/map'


import { Entidade } from "../modelo/entidade.model";
import { ConsultaServico } from "./consulta.service";
import { environment } from "../../../environments/environment";

/**
 * Serviço inerente a manutenção de entidades no backend
 *
 * Referente a interface endpoint ManutencaoEndpoint
 */
export abstract class ManutencaoServico<E extends Entidade> extends ConsultaServico<E> {

    path: string;

  protected buildHttpHeadersJson(): HttpHeaders {
    return new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    });
  }

    /**
     * Construtor do serviço no qual é recebido o path relativo a entidade que será tratada
     *
     * @param path {string} path do endpoint da entidade
     *
     * @param http {Http} serviço http provido pelo sistema subjacente
     */
    constructor(path: string, http: HttpClient) {

        super(path, http)

        this.path = path;
    }
    /**
     * Método responsável por proceder com a chamada do recurso de salvar a entidade parametrizada
     *
     * @param {E} entidade que será persistida
     *
     * @returns {string} mensagem de sucesso
     */
    salvar(entidade: E): Observable<string> {

        return this.http.post(`${environment.api}/${this.path}`, entidade, {headers: this.buildHttpHeaders()}).map(this.mapper)
    }

    /**
     * Método responsável por proceder com a chamada do recurso de alterar a entidade parametrizada
     *
     * @param {E} entidade que será alterada
     *
     * @returns {string} mensagem de sucesso
     */
    alterar(entidade: E): Observable<string> {

        return this.http.put(`${environment.api}/${this.path}`, entidade, {headers: this.buildHttpHeaders()}).map(this.mapper)
    }

    /**
     * Método responsável por excluir a entidade parametrizada pelo seu identificador
     *
     * @param {E} entidade que será alterada
     *
     * @returns {string} mensagem de sucesso
     */
    excluir(entidade: E): Observable<string> {

        return this.http.delete(`${environment.api}/${this.path}/` + entidade.id, {headers: this.buildHttpHeaders()}).map(this.mapper)
    }

    /**
     * Método responsável por alterar o status da entidade parametrizada pelo seu identificador
     *
     * @param {E} entidade que será alterada
     *
     * @returns {string} mensagem de sucesso
     */
    status(entidade: E): Observable<string> {

        return this.http.put(`${environment.api}/${this.path}/` + entidade.id, {headers: this.buildHttpHeaders()}).map(this.mapper)
    }

    /**
     * Método responsável por exportar entidades para excel
     */
    exportar() {

        return this.http.post(`${environment.api}/${this.path}/exportar`, {headers: this.buildHttpHeaders()}).map(this.mapper)
    }

}
