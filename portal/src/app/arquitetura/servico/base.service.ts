
import {HttpClient, HttpErrorResponse, HttpHeaders} from "@angular/common/http";
import {TipoMensagem} from "../modelo";
import {EMPTY} from "rxjs";
import {EventoService} from "./evento.service";


export class BaseService {

  constructor() {
  }

  /**
   * Método responsável por construir os cabeçalhos de requisição padrão nas chamadas do serviço
   *
   * @returns {Headers} cabeçalhos padrão
   */
    protected buildHttpHeaders(): HttpHeaders {

      return new HttpHeaders({
        'Content-Type': 'application/json',

        'Accept': 'application/json'
      });
    }

    /**
     * Método responsável por fazer o mapper da resposta do servidor
     */
    protected mapper(resp) {
        try {
            return (resp.json() || resp.text())
        }catch (error){
            return resp;
        }
    }

}
