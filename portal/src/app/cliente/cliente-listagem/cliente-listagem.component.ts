import {Component, OnInit} from '@angular/core';
import {ConsultaComponent} from "../../arquitetura/componentes";
import {UsuarioCliente} from "../../modelo/usuario-cliente.model";
import {ConfirmationService} from "primeng/api";
import {ActivatedRoute, Router} from "@angular/router";
import {EventoService} from "../../arquitetura/servico";
import {ClienteService} from "../cliente.service";
import {Usuario} from "../../modelo/usuario.model";
import {Status} from "../../modelo/status.enum";
import {StatusTitulo} from "../../modelo/status-titulo.enum";

@Component({
  selector: 'app-cliente-listagem',
  templateUrl: './cliente-listagem.component.html',
  styleUrls: ['./cliente-listagem.component.css']
})
export class ClienteListagemComponent extends  ConsultaComponent<UsuarioCliente> implements OnInit {

  private cliente : UsuarioCliente;

  constructor(

    protected service: ClienteService,

    confirmationService: ConfirmationService,

    router: Router,

    activatedRoute: ActivatedRoute,

    eventoService: EventoService) {

    super(service, confirmationService, router, activatedRoute, eventoService);

  }

  ngOnInit() {
    super.ngOnInit();
  }

  excluir(entidade: UsuarioCliente) {
    if (entidade.status.toUpperCase() === Status.Ativo.toUpperCase() ) {
      super.excluir(entidade);
    }
  }

  clear(){
    this.newEntidade();
    this.entidadeConsulta =  this.cliente;
    super.listar();
  }

  protected newEntidade(): UsuarioCliente {
    this.cliente = new UsuarioCliente();
    this.cliente.administrador = true;
    return this.cliente;
  }

}
