import { Injectable } from '@angular/core';
import {ManutencaoServico} from "../arquitetura/servico";
import {Usuario} from "../modelo/usuario.model";
import {HttpClient} from "@angular/common/http";
import {UsuarioCliente} from "../modelo/usuario-cliente.model";

@Injectable({
  providedIn: 'root'
})
export class ClienteService extends ManutencaoServico<UsuarioCliente>{

  constructor(http: HttpClient) {
    super('cliente',http);
  }
}
