import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ClienteCadastroComponent} from './cliente-cadastro/cliente-cadastro.component';
import {ClienteListagemComponent} from './cliente-listagem/cliente-listagem.component';
import {ArquiteturaModule} from "../arquitetura/arquitetura.module";
import {ClienteRoute} from "./cliente.route";
import {ClienteService} from "./cliente.service";

@NgModule({
  imports: [
    CommonModule,
    ArquiteturaModule,
    ClienteRoute
  ],
  providers:[ClienteService],
  declarations: [ClienteCadastroComponent, ClienteListagemComponent]
})
export class ClienteModule { }
