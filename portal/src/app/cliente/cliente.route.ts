import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ClienteListagemComponent} from "./cliente-listagem/cliente-listagem.component";
import {ClienteCadastroComponent} from "./cliente-cadastro/cliente-cadastro.component";
import {AuthAdmGuard} from "../guard/auth-adm.guard";


export const routes: Routes = [
  { path: 'cliente',    component: ClienteListagemComponent  ,canActivate:[AuthAdmGuard]},
  { path: 'cliente/edicao/:id',    component: ClienteCadastroComponent  ,canActivate:[AuthAdmGuard]}
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClienteRoute  {
}
