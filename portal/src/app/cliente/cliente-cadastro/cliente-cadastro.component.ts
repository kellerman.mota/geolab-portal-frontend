import {Component} from '@angular/core';
import {ManutencaoComponent} from "../../arquitetura/componentes";
import {UsuarioCliente} from "../../modelo/usuario-cliente.model";
import {Usuario} from "../../modelo/usuario.model";
import {AdministradorService} from "../../administrador/administrador.service";
import {LoginService} from "../../login/login.service";
import {ConfirmationService} from "primeng/api";
import {ActivatedRoute, Router} from "@angular/router";
import {EventoService} from "../../arquitetura/servico";
import {ClienteService} from "../cliente.service";
import {Status} from "../../modelo/status.enum";

@Component({
  selector: 'app-cliente-cadastro',
  templateUrl: './cliente-cadastro.component.html',
  styleUrls: ['./cliente-cadastro.component.css']
})
export class ClienteCadastroComponent  extends ManutencaoComponent<UsuarioCliente> {


  private cliente: UsuarioCliente;
  status = [];

  constructor(
    protected service: ClienteService,
    protected loginService: LoginService,
    confirmationService: ConfirmationService,
    router: Router,
    activatedRoute: ActivatedRoute,
    eventoService: EventoService,
  ) {
    super(service, confirmationService, router, activatedRoute, eventoService);
  }

  ngOnInit() {
    this.status = [
      {label: "Ativo", value: Status.Ativo},
      {label: "Inativo", value: Status.Inativo}
    ];
    super.ngOnInit();
  }

  redefinirSenha() {
    this.confirmationService.confirm({

      message: 'Deseja redefinir a senha desse usuário?',
      header: 'Confirmação',

      accept: () => {
        this.loginService.recuperarSenha(this.entidade.login, this.entidade.email).subscribe((retorno: any) => {
          this.adicionarMensagemInformativa(retorno.mensagem);
        })
      }

    })
  }

  protected newEntidade(): UsuarioCliente {
    this.cliente = new UsuarioCliente();
    this.cliente.administrador = false;
    this.cliente.status = Status.Ativo;
    return this.cliente;
  }


}
