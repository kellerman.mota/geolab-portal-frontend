import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import { NgModule, LOCALE_ID } from '@angular/core';
import {AppComponent} from "./app.component";
import {LoginModule} from "./login/login.module";
import {FormsModule}   from '@angular/forms';
import {HttpClientModule, HttpClient, HttpHandler, HTTP_INTERCEPTORS} from "@angular/common/http";
import { registerLocaleData } from '@angular/common';
import localePt from '@angular/common/locales/pt';


import {AppRoutes} from "./app.routes";
import {HomeComponent} from './home/home.component';
import {AuthGuard} from "./guard/auth.guard";
import {ArquiteturaModule} from "./arquitetura/arquitetura.module";
import {TitulosModule} from "./titulos/titulos.module";
import {AuthAdmGuard} from "./guard/auth-adm.guard";
import {Router} from "@angular/router";
import {EventoService} from "./arquitetura/servico";
import {AuthInterceptor} from "./arquitetura/auth-Interceptor";
import {AlterarPerfilModule} from "./alterar-perfil/alterar-perfil.module";
import { MenuSuperiorComponent } from './menu-superior/menu-superior.component';
import { MenuLateralComponent } from './menu-lateral/menu-lateral.component';
import { ConfiguracoesComponent } from './configuracoes/configuracoes.component';
import {AdministradorModule} from "./administrador/administrador.module";
import {ClienteModule} from "./cliente/cliente.module";

registerLocaleData(localePt, 'pt-BR');


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    MenuSuperiorComponent,
    MenuLateralComponent,
    ConfiguracoesComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    LoginModule,
    FormsModule,
    AppRoutes,
    HttpClientModule,
    ArquiteturaModule,
    TitulosModule,
    AlterarPerfilModule,
    AdministradorModule,
    ClienteModule
  ],
  providers: [
    AuthGuard,
    MenuSuperiorComponent,
    AuthAdmGuard,
    { provide: LOCALE_ID, useValue: "pt-BR" },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true,
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
