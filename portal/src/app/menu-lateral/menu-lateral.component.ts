import { Component, OnInit } from '@angular/core';
import {getUsuarioLogado} from "../login/login.service";

@Component({
  selector: 'menu-lateral',
  templateUrl: './menu-lateral.component.html',
  styleUrls: ['./menu-lateral.component.css']
})
export class MenuLateralComponent implements OnInit {

  isAdministrador : boolean;

  constructor() { }

  ngOnInit() {
    this.isAdministrador = getUsuarioLogado().administrador;
  }

}
