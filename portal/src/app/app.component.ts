import {Component, Inject, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {LoginService, usuarioLogado} from "./login/login.service";
import {BehaviorSubject, Observable, Subscription} from "rxjs";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {

  loggedIn: boolean;

  private logoutSubscricao: Subscription;

  private loginSubscricao: Subscription;

  constructor(
    private router: Router,
    private loginService: LoginService) {
  }

  ngOnInit() {
    this.loggedIn = usuarioLogado() != null;
    this.loginSubscricao = this.loginService.loginEvent.subscribe((valor) => {
        this.loggedIn = valor;
        console.log("Fez Loginnnnnnnn!")
      }
    );
    this.logoutSubscricao = this.loginService.logoutEvent.subscribe((valor) => {
        this.loggedIn = valor;
        console.log("Fez LOGOUT!")
      }
    );

  }
}
