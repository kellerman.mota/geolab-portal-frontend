import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {DOCUMENT} from "@angular/common";
import {getUsuarioLogado, LoginService} from "../login/login.service";
import {AlterarPerfilModalComponent} from "../alterar-perfil/alterar-perfil-modal/alterar-perfil-modal.component";
import {AppComponent} from "../app.component";
import {Usuario} from "../modelo/usuario.model";

@Component({
  selector: 'menu-superior',
  templateUrl: './menu-superior.component.html',
  styleUrls: ['./menu-superior.component.css']
})
export class MenuSuperiorComponent implements OnInit {

  @ViewChild("alterarPerfil") alterarPerfil: AlterarPerfilModalComponent;

  toggleState: boolean  = true;

  user : Usuario;

  constructor(
    private loginService : LoginService,
    @Inject(DOCUMENT) private document: Document
  ) { }

  toggle(){
    this.toggleState = !this.toggleState;
    if(this.toggleState){
      document.body.classList.add("aside-collapsed");

    }else{
      document.body.classList.remove("aside-collapsed");

    }
  }

  logout(){
   this.loginService.logout();
  }

  showAlterarSenha(){
    this.alterarPerfil.showDialog();
  }

  ngOnInit() {

    this.user = getUsuarioLogado();

    if(this.user.alterarSenha){

      this.showAlterarSenha();

    }
  }

}
