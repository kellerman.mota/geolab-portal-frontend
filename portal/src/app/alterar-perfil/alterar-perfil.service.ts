import { Injectable } from '@angular/core';
import {BaseService} from "../arquitetura/servico/base.service";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {EventoService} from "../arquitetura/servico";
import {Router} from "@angular/router";
import {Usuario} from "../modelo/usuario.model";
import {Observable} from "rxjs";
import {environment} from "../../environments/environment";
import {AlterarPerfil} from "../modelo/alterar-perfil.model";

@Injectable({
  providedIn: 'root'
})
export class AlterarPerfilService extends BaseService{

  constructor(
    private http: HttpClient,
    private eventoService: EventoService,
    private router: Router
  ) {super();}

  protected buildHttpHeadersJson(): HttpHeaders {
    return new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    });
  }

  alterarPerfil(alterarPerfil: AlterarPerfil): Observable<any> {
    return this.http.post(`${environment.api}/administrador/alterarPerfil`, alterarPerfil,{headers: this.buildHttpHeadersJson()}).map(this.mapper);
  }
}
