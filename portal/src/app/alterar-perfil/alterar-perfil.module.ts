import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AlterarPerfilModalComponent } from './alterar-perfil-modal/alterar-perfil-modal.component';
import {AutenticacaoComponent} from "../login/autenticacao/autenticacao.component";
import {ArquiteturaModule} from "../arquitetura/arquitetura.module";

@NgModule({
  imports: [
    CommonModule,
    ArquiteturaModule
  ],
  declarations: [AlterarPerfilModalComponent],
  exports: [AlterarPerfilModalComponent]
})
export class AlterarPerfilModule { }
