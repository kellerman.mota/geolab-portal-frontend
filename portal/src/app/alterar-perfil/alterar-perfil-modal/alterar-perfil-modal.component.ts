import { Component } from '@angular/core';
import {getUsuarioLogado, updateUsuarioLogado, usuarioLogado} from "../../login/login.service";
import {BaseComponent} from "../../arquitetura/componentes";
import {ActivatedRoute} from "@angular/router";
import {EventoService} from "../../arquitetura/servico";
import {AlterarPerfil} from "../../modelo/alterar-perfil.model";
import {AlterarPerfilService} from "../alterar-perfil.service";
import {Usuario} from "../../modelo/usuario.model";



@Component({
  selector: 'alterar-perfil-modal',
  templateUrl: './alterar-perfil-modal.component.html',
  styleUrls: ['./alterar-perfil-modal.component.css']
})
export class AlterarPerfilModalComponent extends BaseComponent {

  alterarPefil: AlterarPerfil = new AlterarPerfil();

  display: boolean = false;

  obrigatorioAlterarSenha: boolean = false;

    constructor(
    activatedRoute: ActivatedRoute,
    eventoService: EventoService,
    private alterarPefilService: AlterarPerfilService
  ){
    super(eventoService,activatedRoute);
    this.obrigatorioAlterarSenha = getUsuarioLogado().alterarSenha;
  }

  ngOnInit() {
  }

  salvar(){

    if(this.alterarPefil.novoEmail && !this.alterarPefil.confirmarNovoEmail){
      this.adicionarMensagemAlerta("Informar a confirmação do e-mail");
      return;
    }

    if(this.alterarPefil.novaSenha && !this.alterarPefil.confirmarNovaSenha){
      this.adicionarMensagemAlerta("Informar a confirmação da senha");
      return;
    }

    this.alterarPefil.login = getUsuarioLogado().login;

    this.alterarPefilService.alterarPerfil(this.alterarPefil).subscribe((resultado) => {
      this.close();
      this.updateUsuarioLogado();
      this.adicionarMensagemInformativa(resultado.mensagem);
    });

  }

  private updateUsuarioLogado() {
    this.obrigatorioAlterarSenha = false;
    const usuarioLogado = getUsuarioLogado();
    usuarioLogado.alterarSenha = this.obrigatorioAlterarSenha;
    updateUsuarioLogado(usuarioLogado);
  }

  showDialog() {
      this.alterarPefil =  new AlterarPerfil();
      this.display = true;
  }

  close() {
    this.display = false;
  }

  isValid(): boolean {
    if(this.obrigatorioAlterarSenha){
      return  this.preencheuSenhaAtualENovaSenhaOuEmail();
    }else{
      return  this.preencheuSenhaAtualENovaSenhaOuEmail();
    }
  }
  preencheuSenhaAtualENovaSenha(): boolean {
    return  this.preencheuSenhaAtual() && this.preencheuNovaSenhaEConformarNovaSenha();
  }

  preencheuSenhaAtualENovaSenhaOuEmail(): boolean {
    return  this.preencheuSenhaAtual()&& (this.preencheuNovaSenhaEConformarNovaSenha()||this.preencheuEmailEConfirmarEmail())
  }

  preencheuEmailEConfirmarEmail(): boolean {
    return  this.alterarPefil.novoEmail &&  this.alterarPefil.novoEmail.length  > 0 &&
      this.alterarPefil.confirmarNovoEmail && this.alterarPefil.confirmarNovoEmail.length > 0
  }

  preencheuNovaSenhaEConformarNovaSenha(): boolean {
    return this.alterarPefil.novaSenha &&  this.alterarPefil.novaSenha.length  > 0 &&
      this.alterarPefil.confirmarNovaSenha && this.alterarPefil.confirmarNovaSenha.length > 0
  }

  preencheuSenhaAtual(): boolean {
    return  this.alterarPefil.senhaAtual   &&  this.alterarPefil.senhaAtual.length > 0;
  }

}
