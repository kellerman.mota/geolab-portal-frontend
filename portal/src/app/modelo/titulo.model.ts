import {Entidade} from "../arquitetura/modelo";
import {StatusTitulo} from "./status-titulo.enum";


export class Titulo extends Entidade{

  idClienteFiltro: number;

  numeroNotaFiscal: string;

  dataEmissao: Date;

  dataVencimento: Date;

  valorNominal: any;

  numeroDocumento: string;

  parcela: string;

  statusTitulo: string;

  emissaoInicial: Date;

  emissaoFinal: Date;

  vencimentoInicial: Date;

  vencimentoFinal: Date;

  valorInicial: any;

  valorFinal: any;

  numeroNotaFiscalFiltro: string;

  constructor() {
    super();
  }
}
