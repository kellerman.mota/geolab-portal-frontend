import { Entidade } from '../arquitetura/modelo/entidade.model';

export class Usuario extends Entidade {

    codigo: string;

    nome: string;

    login: string;

    senha: string;

    email: string;

    administrador : boolean;

    alterarSenha  : boolean;

    pesquisa : string;

    constructor() {
        super();
    }
}
