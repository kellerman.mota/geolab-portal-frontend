import {Usuario} from "./usuario.model";


export class UsuarioCliente extends Usuario {

  codigoSAP: string;

  emailSAP: string;

  cnpj: string;

  inscricaoEstadual: string;

  dataUltimoAcesso: Date;

  constructor() {
    super();
  }
}
