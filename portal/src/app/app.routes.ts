import { NgModule } from '@angular/core';
import {RouterModule, Routes} from "@angular/router";

import {AutenticacaoComponent} from "./login/autenticacao/autenticacao.component";
import {HomeComponent} from "./home/home.component";
import {AuthGuard} from "./guard/auth.guard";
import {AuthAdmGuard} from "./guard/auth-adm.guard";
import {TitulosListagemComponent} from "./titulos/titulos-listagem/titulos-listagem.component";
import {ConfiguracoesComponent} from "./configuracoes/configuracoes.component";
import {AdministradorListagemComponent} from "./administrador/administrador-listagem/administrador-listagem.component";
import {ClienteListagemComponent} from "./cliente/cliente-listagem/cliente-listagem.component";

export const routes: Routes = [
  { path: '', component: AutenticacaoComponent},
  { path: 'login', component: AutenticacaoComponent },
  { path: 'titulos-listagem', component: TitulosListagemComponent,canActivate:[AuthGuard]},
  { path: 'home', component: HomeComponent,canActivate:[AuthAdmGuard]},
  { path: 'configuracoes',    component: ConfiguracoesComponent  ,canActivate:[AuthAdmGuard]},
  { path: 'administrador',    component: AdministradorListagemComponent  ,canActivate:[AuthAdmGuard]},
  { path: 'cliente',    component: ClienteListagemComponent  ,canActivate:[AuthAdmGuard]}
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { useHash: false })
  ],
  exports: [
    RouterModule
  ]
})

export class AppRoutes {}
