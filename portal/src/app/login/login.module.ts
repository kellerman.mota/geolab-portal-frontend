import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule} from "@angular/forms";

import { LoginRoutingModule } from './login-routing.module';
import { AutenticacaoComponent } from './autenticacao/autenticacao.component';
import { RecuperarSenhaComponent } from './recuperar-senha/recuperar-senha.component';
import {ArquiteturaModule} from "../arquitetura/arquitetura.module";

@NgModule({
  imports: [
    CommonModule,
    LoginRoutingModule,
    FormsModule,
    ArquiteturaModule
  ],
  declarations: [AutenticacaoComponent, RecuperarSenhaComponent],
  exports: [AutenticacaoComponent]
})
export class LoginModule { }
