import {Component, forwardRef, Inject, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {clearToken, LoginService, setUsuarioLogado} from "../login.service";
import {Usuario} from "../../modelo/usuario.model";
import {AppComponent} from "../../app.component";
import {EventoService} from "../../arquitetura/servico";
import {TipoMensagem} from "../../arquitetura/modelo";
import {ConfiguracoesService} from "../../configuracoes/configuracoes.service";
import {Configuracoes} from "../../modelo/configuracoes";

@Component({
  selector: 'app-autenticacao',
  templateUrl: './autenticacao.component.html',
  styleUrls: ['./autenticacao.component.css']
})
export class AutenticacaoComponent implements OnInit {

  usuario: Usuario = new Usuario();
  configuracoes: Configuracoes = new Configuracoes();
  retorno: boolean  = false;

  constructor(
    @Inject(forwardRef(() => AppComponent)) private managerComponent: AppComponent,
    private router: Router,
    private loginService: LoginService,
    private configutacaoService: ConfiguracoesService,
  private eventoService: EventoService
  ) {
    this.configutacaoService.obterConfiguracoes().subscribe((resultado) => {
      this.configuracoes = resultado;
    });
  }

  autenticar() {
    this.loginService.autenticar(this.usuario).subscribe((retorno) => {
      this.loginService.registrarLogin(retorno);
      if(retorno.usuario.administrador){
        this.router.navigate(['/home']);
      }else{
        this.router.navigate(['/titulos-listagem']);
      }
    });
  }

  recuperarSenha() {
    this.loginService.currentUsuario = this.usuario;
    this.router.navigate(['/recuperar-senha']);
  }

    isValid(): boolean {
    return this.usuario.login && this.usuario.login.length <= 30 && this.usuario.senha && this.usuario.senha.length <= 255;
  }

  ngOnInit() {
    this.loginService.resetUsuarioLogado();
  }

}
