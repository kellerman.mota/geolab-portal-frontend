import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AutenticacaoComponent} from "./autenticacao/autenticacao.component";
import {RecuperarSenhaComponent} from "./recuperar-senha/recuperar-senha.component";

const routes: Routes = [
  { path: 'login', component: AutenticacaoComponent },
  { path: 'recuperar-senha', component: RecuperarSenhaComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LoginRoutingModule { }
