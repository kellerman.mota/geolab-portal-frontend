import {EventEmitter, Injectable, Output} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/catch';

import {environment} from "../../environments/environment";
import {Usuario} from "../modelo/usuario.model";
import {BaseService} from "../arquitetura/servico/base.service";
import {EventoService} from "../arquitetura/servico";
import {Router} from "@angular/router";

export const LOCAL_STORAGE_ITEM_USER: string = 'portal.geolab.current.user'

export function usuarioLogado() {

  return localStorage.getItem(LOCAL_STORAGE_ITEM_USER)
}

export function getUsuarioLogado(): Usuario {

  const usuarioLogged = JSON.parse(localStorage.getItem(LOCAL_STORAGE_ITEM_USER));

  return usuarioLogged ? usuarioLogged.usuario : undefined;
}

export function setUsuarioLogado(usuario: Usuario) {

  localStorage.setItem(LOCAL_STORAGE_ITEM_USER, JSON.stringify(usuario))
}

export function updateUsuarioLogado(usuario: Usuario) {
  let token = JSON.parse(localStorage.getItem(LOCAL_STORAGE_ITEM_USER));
  token.usuario = usuario;
  localStorage.setItem(LOCAL_STORAGE_ITEM_USER, JSON.stringify(token))
}

export function clearToken() {
  localStorage.removeItem(LOCAL_STORAGE_ITEM_USER)
}

@Injectable({
  providedIn: 'root'
})
export class LoginService extends BaseService {

  @Output() logoutEvent = new EventEmitter<any>(true);
  @Output() loginEvent = new EventEmitter<any>(true);

  currentUsuario: Usuario;


  constructor(
    private http: HttpClient,
    private eventoService: EventoService,
    private router: Router
  ) {
    super();
    this.currentUsuario = new Usuario();
  }

  protected buildHttpHeaders(): HttpHeaders {
    return new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded'
    });
  }

  protected buildHttpHeadersJson(): HttpHeaders {
    return new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    });
  }

  resetUsuarioLogado() {
    this.logoutEvent.emit(false)
    clearToken()
  }

  registrarLogin(usuario:Usuario) {
    this.loginEvent.emit(true)
    setUsuarioLogado(usuario);
  }

  logout() {
    this.resetUsuarioLogado();
    this.router.navigate(['/login']);
  }

  obterBaseEmailPorLogin(usuario: Usuario): Observable<any> {
    return this.http.post(`${environment.api}/recuperar-senha/obterBaseEmailPorLogin`, usuario, {headers: this.buildHttpHeadersJson()}).map(this.mapper);
  }

  recuperarSenha(usuario: String, email:String): Observable<any> {
    return this.http.post(`${environment.api}/recuperar-senha/` + usuario + `/` + email, {headers: this.buildHttpHeadersJson()}).map(this.mapper);
  }

  autenticar(usuario: Usuario): Observable<any> {
    let body = `username=${encodeURI(usuario.login)}&password=${encodeURI(usuario.senha)}`
    return this.http.post(`${environment.api}/auth`, body, {headers: this.buildHttpHeaders()}).map(this.mapper);
  }

}
