import {Component, ElementRef, forwardRef, Inject, OnInit, ViewChild} from '@angular/core';
import {Usuario} from "../../modelo/usuario.model";
import {AppComponent} from "../../app.component";
import {Router} from "@angular/router";
import {LoginService, setUsuarioLogado} from "../login.service";
import {EventoService} from "../../arquitetura/servico";
import {TipoMensagem} from "../../arquitetura/modelo";

@Component({
  selector: 'app-recuperar-senha',
  templateUrl: './recuperar-senha.component.html',
  styleUrls: ['./recuperar-senha.component.css']
})
export class RecuperarSenhaComponent implements OnInit {

  usuario: Usuario = new Usuario();

  confirmarEmail : string;

  @ViewChild("elConfirmarEmail") elConfirmarEmail: ElementRef;
  @ViewChild("elLogin") elLogin: ElementRef;

  constructor(
    @Inject(forwardRef(() => AppComponent)) private managerComponent: AppComponent,
    private router: Router,
    private loginService: LoginService,
    private eventoService: EventoService
  ) {
    this.usuario = this.loginService.currentUsuario;
  }


  ngOnInit() {}

  ngAfterViewInit() {
    this.elLogin.nativeElement.focus();
    if(this.usuario.login){
      this.obterBaseEmailPorLogin();
    }
  }

  obterBaseEmailPorLogin() {
    if(this.usuario.login){
      this.usuario.email = null;
      this.loginService.obterBaseEmailPorLogin(this.usuario).subscribe((resultado) => {
        this.usuario.email = resultado.email;
        this.elConfirmarEmail.nativeElement.focus();
      });
    }
  }

  recuperarSenha() {

    this.loginService.recuperarSenha(this.usuario.login, this.confirmarEmail).subscribe((resultado) => {

      this.eventoService.mensagemEvento.emit({
        mensagem: resultado.mensagem,
        source: this,
        tipo: TipoMensagem.ALERTA
      });

      this.router.navigate(['/login']);

    });
  }

  voltar() {
    this.router.navigate(['/login']);
  }

}
