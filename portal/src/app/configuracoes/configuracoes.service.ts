import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {EventoService} from "../arquitetura/servico";
import {Router} from "@angular/router";
import {AlterarPerfil} from "../modelo/alterar-perfil.model";
import {Observable} from "rxjs";
import {environment} from "../../environments/environment";
import {BaseService} from "../arquitetura/servico/base.service";
import {Usuario} from "../modelo/usuario.model";
import {Configuracoes} from "../modelo/configuracoes";

@Injectable({
  providedIn: 'root'
})
export class ConfiguracoesService extends BaseService{

  constructor(
    private http: HttpClient,
    private eventoService: EventoService,
    private router: Router
  ) {super();}

  protected buildHttpHeadersJson(): HttpHeaders {
    return new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    });
  }

  salvar(configuracoes: Configuracoes): Observable<any> {
    return this.http.post(`${environment.api}/configuracao/salvarConfiguracoes`, configuracoes,{headers: this.buildHttpHeadersJson()}).map(this.mapper);
  }

   obterConfiguracoes(): Observable<any> {
    return this.http.post(`${environment.api}/configuracao/obterConfiguracoes`,{headers: this.buildHttpHeadersJson()}).map(this.mapper);
  }


}
