import {Component, ElementRef, Inject, OnInit, ViewChild} from '@angular/core';
import {BaseComponent} from "../arquitetura/componentes";
import {ActivatedRoute} from "@angular/router";
import {EventoService} from "../arquitetura/servico";
import {ConfiguracoesService} from "./configuracoes.service";
import {DOCUMENT} from "@angular/common";
import {Configuracoes} from "../modelo/configuracoes";
import {ValidadoresComponent} from "../arquitetura/componentes/validador.component";
import {validate} from "codelyzer/walkerFactory/walkerFn";

@Component({
  selector: 'app-configuracoes',
  templateUrl: './configuracoes.component.html',
  styleUrls: ['./configuracoes.component.css']
})
export class ConfiguracoesComponent extends BaseComponent {

  configuracoes: Configuracoes = new Configuracoes();

  constructor(
    activatedRoute: ActivatedRoute,
    eventoService: EventoService,
    private validador: ValidadoresComponent,
    private configutacaoService: ConfiguracoesService
  ) { super(eventoService,activatedRoute);}

  salvar(){

    const emailEmail = this.configuracoes.emailEmail;
    const contatoEmail = this.configuracoes.contatoEmail;

    if(emailEmail && !this.validador.validarEmail(emailEmail)){
      this.adicionarMensagemAlerta("Configurações de e-mail: E-mail inválido")
      return;
    }

    if(contatoEmail && !this.validador.validarEmail(contatoEmail)){
      this.adicionarMensagemAlerta("Contatos: E-mail inválido")
      return;
    }

    this.configutacaoService.salvar(this.configuracoes).subscribe((resultado) => {
      this.adicionarMensagemInformativa(resultado.mensagem);
    });
  }

  ngOnInit() {
    this.configutacaoService.obterConfiguracoes().subscribe((resultado) => {
      this.configuracoes = resultado;
    });
  }

}
