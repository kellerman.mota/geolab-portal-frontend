import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import { Observable } from 'rxjs';
import {getUsuarioLogado, usuarioLogado} from "../login/login.service";

@Injectable({
  providedIn: 'root'
})
export class AuthAdmGuard implements CanActivate {

  constructor(private router: Router) {

  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    const isUsuarioLogged = usuarioLogado();

    const usuarioLogged = getUsuarioLogado();

    if (isUsuarioLogged && usuarioLogged && usuarioLogged.administrador) {
      return true;
    }
    this.router.navigate(['/login']);

    return false;
  }
}
