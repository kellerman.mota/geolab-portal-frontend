import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TitulosListagemComponent} from './titulos-listagem/titulos-listagem.component';
import {AlterarPerfilModule} from '../alterar-perfil/alterar-perfil.module';
import {ArquiteturaModule} from '../arquitetura/arquitetura.module';
import {TitulosService} from './titulos.service';

@NgModule({
  imports: [
    CommonModule,
    AlterarPerfilModule,
    ArquiteturaModule
  ],
  providers: [TitulosService],
  declarations: [TitulosListagemComponent]

})
export class TitulosModule {
}
