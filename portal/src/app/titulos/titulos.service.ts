import {Injectable} from '@angular/core';
import {ManutencaoServico} from '../arquitetura/servico';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Titulo} from '../modelo/titulo.model';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TitulosService extends ManutencaoServico<Titulo> {

  constructor(http: HttpClient) {
    super('titulo', http);
  }

  protected buildHttpHeadersPDF(): HttpHeaders {
    return;
  }

  downloadTituloBoleto(titulo: Titulo): any {

    const headers = new HttpHeaders({
      'Content-Type': 'application/pdf',
      'Accept': 'application/pdf'
    });

    return this.http.post(`${environment.api}/titulo/downloadTituloBoleto/` + titulo.id, null,
      {
        headers: headers,
        responseType: 'blob'
      }
    ).map(response => {
      return new Blob([response], {type: 'application/pdf'});

    });
  }

  downloadTituloBarras(titulo: Titulo): any {

    const headers = new HttpHeaders({
      'Content-Type': 'application/pdf',
      'Accept': 'application/pdf'
    });

    return this.http.post(`${environment.api}/titulo/downloadTituloBarras/` + titulo.id, null,
      {
        headers: headers,
        responseType: 'blob'
      }
    ).map(response => {
      return new Blob([response], {type: 'application/pdf'});

    });
  }

  downloadTituloPdf(titulo: Titulo): any {

    const headers = new HttpHeaders({
      'Content-Type': 'application/pdf',
      'Accept': 'application/pdf'
    });

    return this.http.post(`${environment.api}/titulo/downloadTituloPDF/` + titulo.id, null,
      {
        headers: headers,
        responseType: 'blob'
      }
    ).map(response => {
      return new Blob([response], {type: 'application/pdf'});

    });
  }

  downloadTituloXLS(titulo: Titulo): any {

    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept': 'application/octet-stream'
    });

    return this.http.post(`${environment.api}/titulo/downloadTituloXLS/` + titulo.id, null,
      {headers: headers, responseType: 'blob'}
    ).map(response => {
      return new Blob([response], {type: 'application/octet-stream'});

    });

  }

}
