import {Component, OnInit} from '@angular/core';
import {ConsultaComponent} from '../../arquitetura/componentes';
import {Titulo} from '../../modelo/titulo.model';
import {ConfirmationService} from 'primeng/api';
import {ActivatedRoute, Router} from '@angular/router';
import {EventoService} from '../../arquitetura/servico';
import {TitulosService} from '../titulos.service';
import {StatusTitulo} from '../../modelo/status-titulo.enum';
import {getUsuarioLogado} from '../../login/login.service';

@Component({
  selector: 'app-titulos-listagem',
  templateUrl: './titulos-listagem.component.html',
  styleUrls: ['./titulos-listagem.component.css']
})
export class TitulosListagemComponent extends ConsultaComponent<Titulo> implements OnInit {

  private titulo: Titulo;

  statusTitulo = [];


  constructor(
    protected service: TitulosService,
    confirmationService: ConfirmationService,
    router: Router,
    activatedRoute: ActivatedRoute,
    eventoService: EventoService) {

    super(service, confirmationService, router, activatedRoute, eventoService);

  }

  ngOnInit() {
    this.statusTitulo = [
      {label: 'Selecione...', value: null},
      {label: 'Em Aberto', value: StatusTitulo.EM_ABERTO},
      {label: 'Vencido', value: StatusTitulo.VENCIDO}
    ];
    super.ngOnInit();
  }

  clear() {
    this.newEntidade();
    this.entidadeConsulta = this.titulo;
    super.listar();
  }

  protected newEntidade(): Titulo {
    this.titulo = new Titulo();
    this.titulo.idClienteFiltro = getUsuarioLogado().id;
    return this.titulo;
  }

  getTooltipTitulo(obj: Titulo): any {
    return 'Nr. documento: ' + obj.numeroDocumento + ' Parcela: ' + obj.parcela;
  }

  downloadBarras(obj: Titulo) {
    this.service.downloadTituloBarras(obj).subscribe(
      (res) => {
        const url = URL.createObjectURL(res);
        const a = document.createElement('a');
        a.setAttribute('style', 'display:none;');
        a.setAttribute('href', url);
        a.setAttribute('download', obj.numeroNotaFiscal + '.pdf');
        document.body.appendChild(a);
        a.click();
        URL.revokeObjectURL(url);
        a.parentNode.removeChild(a);

      }
    );
  }

  downloadBoleto(obj: Titulo) {
    this.service.downloadTituloBoleto(obj).subscribe(
      (res) => {
        const url = URL.createObjectURL(res);
        const a = document.createElement('a');
        a.setAttribute('style', 'display:none;');
        a.setAttribute('href', url);
        a.setAttribute('download', obj.numeroNotaFiscal + '.pdf');
        document.body.appendChild(a);
        a.click();
        URL.revokeObjectURL(url);
        a.parentNode.removeChild(a);
      }
    );
  }

  downloadPDF(obj: Titulo) {
    this.service.downloadTituloPdf(obj).subscribe(
      (res) => {
        const url = URL.createObjectURL(res);
        const a = document.createElement('a');
        a.setAttribute('style', 'display:none;');
        a.setAttribute('href', url);
        a.setAttribute('download', obj.numeroNotaFiscal + '.pdf');
        document.body.appendChild(a);
        a.click();
        URL.revokeObjectURL(url);
        a.parentNode.removeChild(a);

      }
    );
  }

  downloadXLS(obj: Titulo) {
    this.service.downloadTituloXLS(obj).subscribe(
      (res) => {
        const url = URL.createObjectURL(res);
        const a = document.createElement('a');
        a.setAttribute('style', 'display:none;');
        a.setAttribute('href', url);
        a.setAttribute('download', obj.numeroNotaFiscal + '.xls');
        document.body.appendChild(a);
        a.click();
        URL.revokeObjectURL(url);
        a.parentNode.removeChild(a);

      }
    );
  }

}
