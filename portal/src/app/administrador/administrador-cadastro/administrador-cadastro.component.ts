import {Component, OnInit} from '@angular/core';
import {ManutencaoComponent} from '../../arquitetura/componentes';
import {Usuario} from '../../modelo/usuario.model';
import {ActivatedRoute, Router} from '@angular/router';
import {EventoService} from '../../arquitetura/servico';
import {AdministradorService} from '../administrador.service';
import {ConfirmationService} from 'primeng/api';
import {Status} from '../../modelo/status.enum';
import {LoginService} from '../../login/login.service';

@Component({
  selector: 'app-administrador-cadastro',
  templateUrl: './administrador-cadastro.component.html',
  styleUrls: ['./administrador-cadastro.component.css']
})
export class AdministradorCadastroComponent extends ManutencaoComponent<Usuario> implements OnInit {

  private usuario: Usuario;
  status = [];

  constructor(
    protected service: AdministradorService,
    protected loginService: LoginService,
    confirmationService: ConfirmationService,
    router: Router,
    activatedRoute: ActivatedRoute,
    eventoService: EventoService,
  ) {
    super(service, confirmationService, router, activatedRoute, eventoService);
  }

  ngOnInit() {
    this.status = [
      {label: 'Ativo', value: Status.Ativo},
      {label: 'Inativo', value: Status.Inativo}
    ];
    super.ngOnInit();
  }

  redefinirSenha() {
    this.confirmationService.confirm({

      message: 'Deseja redefinir a senha desse usuário?',
      header: 'Confirmação',

      accept: () => {
        this.loginService.recuperarSenha(this.entidade.login, this.entidade.email).subscribe((retorno: any) => {
          this.adicionarMensagemInformativa(retorno.mensagem);
        });
      }

    });
  }

  protected newEntidade(): Usuario {
    this.usuario = new Usuario();
    this.usuario.administrador = true;
    this.usuario.status = Status.Ativo;
    return this.usuario;
  }


}
