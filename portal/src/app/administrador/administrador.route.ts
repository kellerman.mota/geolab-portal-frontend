import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AdministradorListagemComponent} from "./administrador-listagem/administrador-listagem.component";
import {AdministradorCadastroComponent} from "./administrador-cadastro/administrador-cadastro.component";
import {AuthAdmGuard} from "../guard/auth-adm.guard";


export const routes: Routes = [
  { path: 'administrador',    component: AdministradorListagemComponent  ,canActivate:[AuthAdmGuard]},
  { path: 'administrador/novo',    component: AdministradorCadastroComponent  ,canActivate:[AuthAdmGuard]},
  { path: 'administrador/edicao/:id',    component: AdministradorCadastroComponent  ,canActivate:[AuthAdmGuard]}
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdministradorRoute  {
}
