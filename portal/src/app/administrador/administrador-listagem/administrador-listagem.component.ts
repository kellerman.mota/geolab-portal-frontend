import {Component, OnInit} from '@angular/core';
import {ConsultaComponent} from "../../arquitetura/componentes";
import {Usuario} from "../../modelo/usuario.model";
import {AdministradorService} from "../administrador.service";
import {ConfirmationService} from "primeng/api";
import {ActivatedRoute, Router} from "@angular/router";
import {EventoService} from "../../arquitetura/servico";
import {Tipo} from "../../arquitetura/modelo";
import {Status} from "../../modelo/status.enum";

@Component({
  selector: 'app-administrador-listagem',
  templateUrl: './administrador-listagem.component.html',
  styleUrls: ['./administrador-listagem.component.css']
})
export class AdministradorListagemComponent extends ConsultaComponent<Usuario> implements OnInit {

  private usuario: Usuario;

  constructor(
    protected service: AdministradorService,
    confirmationService: ConfirmationService,
    router: Router,
    activatedRoute: ActivatedRoute,
    eventoService: EventoService) {

    super(service, confirmationService, router, activatedRoute, eventoService);

  }

  ngOnInit() {
    super.ngOnInit();
  }

  clear() {
    this.newEntidade();
    this.entidadeConsulta = this.usuario;
    super.listar();
  }

  excluir(entidade: Usuario) {
    if (entidade.status.toUpperCase() === Status.Ativo.toUpperCase() ) {
      super.excluir(entidade);
    }
  }

  protected newEntidade(): Usuario {
    this.usuario = new Usuario();
    this.usuario.administrador = true;
    return this.usuario;
  }

}
