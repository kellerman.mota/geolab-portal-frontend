import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdministradorListagemComponent } from './administrador-listagem/administrador-listagem.component';
import { AdministradorCadastroComponent } from './administrador-cadastro/administrador-cadastro.component';
import {ArquiteturaModule} from "../arquitetura/arquitetura.module";
import {AdministradorRoute} from "./administrador.route";
import {AdministradorService} from "./administrador.service";

@NgModule({
  imports: [
    CommonModule,
    AdministradorRoute,
    ArquiteturaModule
  ],
  providers:[AdministradorService],
  declarations: [AdministradorListagemComponent, AdministradorCadastroComponent]
})
export class AdministradorModule { }
