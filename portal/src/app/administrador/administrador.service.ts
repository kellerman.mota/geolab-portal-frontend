import { Injectable } from '@angular/core';
import {Usuario} from "../modelo/usuario.model";
import {ManutencaoServico} from "../arquitetura/servico";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class AdministradorService extends ManutencaoServico<Usuario>{

  constructor(http: HttpClient) {
    super('administrador',http);
  }

  enviarSenhaEmail(usuario: Usuario): Observable<any> {
    return this.http.post(`${environment.api}/recuperar-senha/envia-email-senha`, usuario, {headers: this.buildHttpHeadersJson()}).map(this.mapper)
  }

}
