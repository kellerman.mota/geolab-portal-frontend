export const environment = {
  production: true,   
  baseUrl: 'http://localhost:8080/portal/',

  api: 'http://localhost:8080/portal/api'
};
